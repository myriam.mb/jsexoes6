
// Exercice 1
export function exercice1 () {

  for (let a = 0; a < 10 ; a++) {
    console.log(a);
    
  }
};


// Exercice 2
export function exercice2 () {

  let b = 41;
  for (let a = 0; a <= 20 ; a++) {
    
    console.log(a * b);
    
  }
};


// Exercice 3
export function exercice3 () {

  let b = 1;
  for (let a = 100; a >= 10 ; a--) {
    
    console.log(a * b);
    
  }
};


// Exercice 4
export function exercice4 () {

  
  for (let a = 1; a < 10 ; a = a + 1/2) {
    
    console.log(a);
    
  }
};


// Exercice 5
export function exercice5 () {

  
  for (let a = 1; a <= 15 ; a++) {
    
    console.log(a + ":" + " On y arrive presque...");
    
  }
};


// Exercice 6
export function exercice6 () {

  
  for (let a = 20; a >= 0 ; a--) {
    
    console.log(a + ":" + " C'est presque bon...");
    
  }
};


// Exercice 7
export function exercice7 () {

  
  for (let a = 0 ; a <= 100 ; a = a + 15) {
    
    console.log(a + ":" + " On tient le bon bout...");
    
  }
};


// Exercice 8
export function exercice8 () {

  
  for (let a = 200; a >= 0 ; a = a - 12) {
    
    console.log(a + ":" + " Enfin ! ! !");
    
  }
};


   

        





