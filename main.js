import * as exo from './Exercices/exo1.js'
import * as exo2 from './Exercices/exo2.js'
import * as exo3 from './Exercices/exo3.js'


  // Exercice I

console.log('Exercice 1:');
exo.exercice1();

console.log('Exercice 2:');
exo.exercice2();

console.log('Exercice 3:');
exo.exercice3();

console.log('Exercice 4:');
exo.exercice4();

console.log('Exercice 5:');
exo.exercice5();

console.log('Exercice 6:');
exo.exercice6();

console.log('Exercice 7:');
exo.exercice7();

console.log('Exercice 8:');
exo.exercice8();


// Exercice II

console.log('Exercice 1:');
exo2.exercice1();

console.log('Exercice 2:');
exo2.exercice2();

console.log('Exercice 3:');
exo2.exercice3();

console.log('Exercice 4:');
exo2.exercice4();

console.log('Exercice 5:');
exo2.exercice5();

console.log('Exercice 6:');
exo2.exercice6();

console.log('Exercice 7:');
exo2.exercice7();

console.log('Exercice 8:');
exo2.exercice8();


// Exercices III

console.log('Exercice 1:');
console.log(exo3.tableauMois);

console.log('Exercice 2:');
exo3.exercice2(exo3.tableauMois);

console.log('Exercice 3:');
exo3.exercice3(exo3.tableauMois);

console.log('Exercice 4:');
exo3.exercice4(exo3.tableauMois);

console.log('Exercice 5:');
console.log(exo3.tableauDep);

console.log('Exercice 6:');
exo3.exercice6(exo3.tableauDep);

console.log('Exercice 7:');
exo3.exercice7(exo3.tableauDep);

console.log('Exercice 8:');
console.log(exo3.tableauMois);

console.log('Exercice 9:');
exo3.exercice9(exo3.tableauDep);

console.log('Exercice 10:');
exo3.exercice10(exo3.tableauDep);
